package com.amplastudio.eoquetem.recipe;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.amplastudio.eoquetem.R;
import com.amplastudio.eoquetem.recipe.adapter.RecipeRecyclerViewAdapter;
import com.amplastudio.eoquetem.recipe.res.Recipe;

/**
 * Class that handles activity to show a recipe to the user.
 */

public class RecipeActivity extends AppCompatActivity {

    /**
     * String to be used as a key in {@link android.content.Intent#putExtra(String, Parcelable)}
     */
    public static final String EXTRA_RECIPE = "extra_recipe";

    @Override
    public void onCreate(Bundle savedInstaceState){
        super.onCreate(savedInstaceState);
        setContentView(R.layout.activity_recipe);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.activity_recipe_recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        Recipe recipe = getIntent().getExtras().getParcelable(EXTRA_RECIPE);
        //recyclerView.addItemDecoration(new RecipeRecyclerViewAdapter
         //       .SelectableDividerDecorator(this, new int[]{recipe.getIngredients().size()}));
        recyclerView.setAdapter(new RecipeRecyclerViewAdapter(this, recipe));

        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_recipe_toolbar);
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.colorToolbarTitle));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(recipe.getName());

    }

}
