package com.amplastudio.eoquetem.recipe.res;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.util.ArrayList;

/**
 * Class which contains details about one chef
 * @author Murillo Henrique Pedroso Ferreira.
 */
public class Chef implements Parcelable{

    private final ArrayList<SocialNetwork> socialNetworks;

    private String name;

    /**
     *
     * @param name Chef's name.
     */
    public Chef(String name){
        this.name = name;
        socialNetworks = new ArrayList<>();
    }

    /**
     * Should not be used, this constructor is intent to be used by Android operational system only.
     * @param in Parcel to read from and initialize variable values.
     */
    protected Chef(Parcel in) {
        socialNetworks = in.createTypedArrayList(SocialNetwork.CREATOR);
        name = in.readString();
    }

    /**
     * Needed for parcelable implementation.
     */
    public static final Creator<Chef> CREATOR = new Creator<Chef>() {
        @Override
        public Chef createFromParcel(Parcel in) {
            return new Chef(in);
        }

        @Override
        public Chef[] newArray(int size) {
            return new Chef[size];
        }
    };

    /**
     *
     * @return Chef's name.
     */
    public @NonNull String getName(){
        return name;
    }

    /**
     * @param socialNetwork Social network to add.
     */
    public void addSocialNetwork(SocialNetwork socialNetwork){
        this.socialNetworks.add(socialNetwork);
    }

    /**
     *
     * @return List of socialNetworks of this chef.
     */
    public ArrayList<SocialNetwork> getSocialNetworks() {
        return socialNetworks;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        SocialNetwork[] socialNetworks = new SocialNetwork[this.socialNetworks.size()];
        socialNetworks = this.socialNetworks.toArray(socialNetworks);

        dest.writeTypedArray(socialNetworks, 0);
        dest.writeString(name);

    }

}
