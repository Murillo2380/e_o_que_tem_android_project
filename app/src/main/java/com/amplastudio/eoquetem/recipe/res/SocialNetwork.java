package com.amplastudio.eoquetem.recipe.res;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

/**
 * Class that defines attributes of a social network.
 */
public class SocialNetwork implements Parcelable {

    /**
     * Icon id for this social network.
     */
    private int iconId;

    /**
     * Link to social network.
     */
    private String url;

    /**
     *
     * @param iconId Icon ID for this social network.
     * @param url Link to social network.
     */
    public SocialNetwork(int iconId, @NonNull String url){
        this.iconId = iconId;
        this.url = url;
    }

    /**
     * Should not be used, this constructor is intent to be used by Android operational system only.
     * @param in Parcel to read from and initialize variable values.
     */
    protected SocialNetwork(Parcel in) {
        iconId = in.readInt();
        url = in.readString();
    }

    /**
     * Needed for parcelable implementation.
     */
    public static final Creator<SocialNetwork> CREATOR = new Creator<SocialNetwork>() {
        @Override
        public SocialNetwork createFromParcel(Parcel in) {
            return new SocialNetwork(in);
        }

        @Override
        public SocialNetwork[] newArray(int size) {
            return new SocialNetwork[size];
        }
    };

    /**
     *
     * @return Res ID of the icon for this social network.
     */
    public int getIconId() {
        return iconId;
    }

    /**
     *
     * @return Link to social network.
     */
    public @NonNull String getUrl() {
        return url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(iconId);
        dest.writeString(url);
    }
}
