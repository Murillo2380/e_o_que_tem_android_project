package com.amplastudio.eoquetem.recipe.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amplastudio.eoquetem.R;
import com.amplastudio.eoquetem.recipe.res.Recipe;

import java.util.Locale;

/**
 * Adapter that handles a {@link RecyclerView} to show a {@link Recipe} to the user.
 * @author Murillo Henrique Pedroso Ferreira
 */

public class RecipeRecyclerViewAdapter extends RecyclerView.Adapter<RecipeRecyclerViewAdapter.ViewHolder> {

    /**
     *  If {@link ViewHolder#viewType} is equal to this value then the content of the list
     *  will be the value in {@link #directionTitle}
     */
    private static final int VIEW_TYPE_DIRECTION_TITLE = 0;

    /**
     *  If {@link ViewHolder#viewType} is equal to this value then the content of the list
     *  will be the value in {@link #ingredientTitle}
     */
    private static final int VIEW_TYPE_INGREDIENT_TITLE = 1;

    /**
     *  If {@link ViewHolder#viewType} is equal to this value then the content of the list
     *  will be one of the values in {@link Recipe#getDirections()}
     */
    private static final int VIEW_TYPE_DIRECTION_BODY = 2;

    /**
     *  If {@link ViewHolder#viewType} is equal to this value then the content of the list
     *  will be one of the values in {@link Recipe#getIngredients()}
     */
    private static final int VIEW_TYPE_INGREDIENT_BODY = 3;

    /**
     * Recipe that must be shown.
     */
    private final Recipe recipe;

    /**
     * String loaded from String resources. Will be {@code "Directions"} translated to
     * the user's language.
     */
    private final String directionTitle;

    /**
     * String loaded from String resources. Will be {@code Ingredients} translated to
     * the user's language.
     */
    private final String ingredientTitle;

    private final Resources res;

    private ColorStateList defaultTextViewColor;

    class ViewHolder extends RecyclerView.ViewHolder{

        /**
         * TextView which will be used to show the recipe's ingredients and directions.
         */
        final TextView textView;

        /**
         * Type of this view. Will be either {@link #VIEW_TYPE_DIRECTION_TITLE}
         * {@link #VIEW_TYPE_DIRECTION_BODY} {@link #VIEW_TYPE_INGREDIENT_TITLE} or
         * {@link #VIEW_TYPE_INGREDIENT_BODY}
         */
        final int viewType;

        /**
         * Root view of the layout.
         */
        final View root;

        /**
         *
         * @param view Inflated view.
         * @param viewType Type of view, must be either {@link #VIEW_TYPE_DIRECTION_TITLE}
         * {@link #VIEW_TYPE_DIRECTION_BODY} {@link #VIEW_TYPE_INGREDIENT_TITLE} or
         * {@link #VIEW_TYPE_INGREDIENT_BODY}
         */
        ViewHolder(View view, int viewType){
            super(view);
            root = view;
            textView = (TextView) view.findViewById(R.id.recipe_activity_list_item_textview);
            this.viewType = viewType;

            if(defaultTextViewColor == null)
                defaultTextViewColor = textView.getTextColors();

        }

    }

    /**
     * Creates a list containing information about the given recipe.
     * @param recipe Recipe to be shown.
     */
    public RecipeRecyclerViewAdapter(Context context, Recipe recipe){
        this.recipe = recipe;
        directionTitle = context.getResources().getString(R.string.activity_recipe_directions_title);
        ingredientTitle = context.getResources().getString(R.string.activity_recipe_ingredients_title);
        res = context.getResources();
        defaultTextViewColor = null;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View root = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recipe_activity_list_item, parent, false);

        return new ViewHolder(root, viewType);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        switch (holder.viewType){

            case VIEW_TYPE_INGREDIENT_TITLE:
                holder.textView.setText(ingredientTitle);
                holder.textView.setTextColor(ContextCompat.getColor(holder.textView.getContext(),
                        R.color.recipeTitles));
                break;

            case VIEW_TYPE_INGREDIENT_BODY:
                holder.textView.setText(recipe.getIngredients().get(position - 1));
                holder.textView.setTextColor(defaultTextViewColor);
                break;

            case VIEW_TYPE_DIRECTION_TITLE:
                holder.textView.setText(directionTitle);
                holder.textView.setTextColor(ContextCompat.getColor(holder.textView.getContext(),
                        R.color.recipeTitles));
                break;

            case VIEW_TYPE_DIRECTION_BODY:
                holder.textView.setText(recipe.getDirections()
                        .get(position - recipe.getIngredients().size() - 2));

                holder.textView.setText(String.format(
                        res.getString(R.string
                                .activity_recipe_list_tem_ingredents_and_directions_format),
                        position - recipe.getIngredients().size() - 1,
                        recipe.getDirections()
                                .get(position - recipe.getIngredients().size() - 2)));

                holder.textView.setTextColor(defaultTextViewColor);
                break;

        }


    }

    @Override
    public int getItemCount() {
        //+2 for ingredient and directions title.
        return recipe.getIngredients().size() + recipe.getDirections().size() + 2;
    }

    @Override
    public int getItemViewType(int position){

        if(position == 0)
            return VIEW_TYPE_INGREDIENT_TITLE;

        if(position <= recipe.getIngredients().size())
            return VIEW_TYPE_INGREDIENT_BODY;

        if(position == recipe.getIngredients().size() + 1)
            return VIEW_TYPE_DIRECTION_TITLE;

        return VIEW_TYPE_DIRECTION_BODY;

    }

    /**
     * Class that let you choose which list elements will have a divider.
     */
    public static class SelectableDividerDecorator extends RecyclerView.ItemDecoration{

        /**
         * List divider
         */
        private Drawable divider;

        private int dividersToBePlaced[];

        public static final String TAG_TO_PLACE_DIVIDER = "place_divider";

        /**
         *
         * @param context App context to retrieve divider drawable.
         * @param dividersToBePlaced Positions where dividers will be drawn. Dividers are Drawn
         *                           over the list element, after they are drawn. If {@code null}
         *                           then the childs tagged with {@link #TAG_TO_PLACE_DIVIDER}
         *                           will receive a divider.
         */
        public SelectableDividerDecorator(Context context, @NonNull int dividersToBePlaced[]){
            TypedArray typedArray = context.obtainStyledAttributes(new int[]{android.R.attr.listDivider});
            divider = typedArray.getDrawable(0);
            typedArray.recycle();
            this.dividersToBePlaced = dividersToBePlaced;
        }


        @Override
        public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state){

            View child;
            RecyclerView.LayoutParams params;

            int top;
            int bottom;
            int left = parent.getPaddingLeft();
            int right = parent.getWidth() - parent.getPaddingRight();

            for(int i : dividersToBePlaced){
                child = parent.getChildAt(i);

                if(child == null) // not visible
                    continue;

                params = (RecyclerView.LayoutParams) child.getLayoutParams();

                top = child.getBottom() + params.bottomMargin;
                bottom = top + divider.getIntrinsicHeight();

                divider.setBounds(left, top, right, bottom);
                divider.draw(c);
            }


        }


    }


}
