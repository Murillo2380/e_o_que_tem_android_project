package com.amplastudio.eoquetem.recipe.adapter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.amplastudio.eoquetem.R;
import com.amplastudio.eoquetem.recipe.res.Recipe;

import java.util.ArrayList;

/**
 * Class that instantiate {@link android.support.v7.widget.CardView} to display a brief
 * recipe's description.
 * @author Murillo Henrique Pedroso Ferreira
 */

public class RecipeChooserRecyclerViewAdapter extends RecyclerView.Adapter<RecipeChooserRecyclerViewAdapter.ViewHolder>
        implements Button.OnClickListener{

    /**
     * Class holding the views in {@link android.support.v7.widget.CardView}.
     */
    class ViewHolder extends RecyclerView.ViewHolder{

        final ImageView recipeImage;
        final TextView recipeTitle;
        final TextView ingredients;

        /**
         * Button to select a recipe to see the directions and other information.
         * Each button is tagged with an integer number representing the position in
         * {@link #recipes}, so when a click is detected the button can retrieve the recipe
         * associated to him.
         */
        final Button makeButton;

        ViewHolder(View itemView) {
            super(itemView);

            recipeImage = (ImageView) itemView.findViewById(R.id.recipe_chooser_activity_list_row_item_imageview);
            recipeTitle = (TextView) itemView.findViewById(R.id.recipe_chooser_activity_list_row_item_textview_title);
            ingredients = (TextView) itemView.findViewById(R.id.recipe_chooser_activity_list_row_item_textview_ingredients);
            makeButton = (Button) itemView.findViewById(R.id.recipe_chooser_activity_list_row_item_button_make);
            makeButton.setOnClickListener(RecipeChooserRecyclerViewAdapter.this);

        }

    }

    private ArrayList<Recipe> recipes;
    private OnRecipeChosenListener listener;

    /**
     * Bullet unicode symbol.
     */
    private static final String INGREDIENT_SEPARATOR = "\u2022";


    /**
     *
     * @param listener Listener instance to recive a callback when a recipe is chosen.
     * @param recipes Recipes to be shown.
     * @see #addRecipe(Recipe)
     * @see #addRecipe(Recipe...)
     */
    public RecipeChooserRecyclerViewAdapter(@NonNull OnRecipeChosenListener listener,
                                            @Nullable ArrayList<Recipe> recipes){
        this.recipes = recipes;
        this.listener = listener;
    }

    /**
     * @param recipe Recipe to add. In order to update the {@link RecyclerView}, call
     *               {@link #notifyDataSetChanged()}
     */
    public void addRecipe(Recipe recipe){

        if(recipes == null)
            recipes = new ArrayList<>();

        this.recipes.add(recipe);
    }

    /**
     *
     * @param recipes Recipes to add. In order to update the {@link RecyclerView}, call
     *               {@link #notifyDataSetChanged()}
     */
    public void addRecipe(Recipe... recipes){

        if(this.recipes == null)
            this.recipes = new ArrayList<>();

        for(Recipe r : recipes)
            this.recipes.add(r);
    }

    @Override
    public RecipeChooserRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recipe_chooser_activity_list_row_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecipeChooserRecyclerViewAdapter.ViewHolder holder, int position) {

        Recipe relatedRecipe = recipes.get(position);
        holder.recipeTitle.setText(relatedRecipe.getName());

        StringBuilder ingredientSeparatorBuilder = new StringBuilder();

        for(String ingredient : relatedRecipe.getIngredients()){
            ingredientSeparatorBuilder.append(ingredient)
                    .append(" ")
                    .append(INGREDIENT_SEPARATOR)
                    .append(" ");
        }

        holder.ingredients.setText(ingredientSeparatorBuilder.substring(0,
                ingredientSeparatorBuilder.lastIndexOf(" " + INGREDIENT_SEPARATOR)));

        holder.makeButton.setTag(position);
    }

    @Override
    public int getItemCount() {
        return recipes == null ? 0 : recipes.size();
    }

    @Override
    public void onClick(View v) {
        int position = (int) v.getTag();
        Recipe selectedRecipe = recipes.get(position);
        listener.onRecipeChosen(selectedRecipe);
    }

    /**
     * Interface that listening for the choice of recipes.
     */
    public interface OnRecipeChosenListener{

        /**
         * Called when a recipe is chosen.
         * @param recipe Chosen recipe.
         */
        void onRecipeChosen(Recipe recipe);

    }

}
