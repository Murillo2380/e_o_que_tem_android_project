package com.amplastudio.eoquetem.recipe.res;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.util.ArrayList;

/**
 * Class with basic attributes which defines a recipe. All information was inspired on the recipe
 * in this <a href="http://allrecipes.com/recipe/14344/greek-veggie-salad/?internalSource=rotd&referringId=1947&referringContentType=recipe%20hub&clickId=cardslot%201">link</a>
 * @author Murillo Henrique Pedroso Ferreira
 */
public class Recipe implements Parcelable{

    /**
     * Holds a list of ingredients to cook this recipe
     */
    private final ArrayList<String> ingredients;

    /**
     * Holds a list of instructions to cook this recipe
     */
    private final ArrayList<String> directions;

    /**
     * List of owners of this recipe
     */
    private final ArrayList<Chef> recipeOwners;

    /**
     * Time to prepare this recipe in minutes
     */
    private int preparationTime;

    /**
     * Number of persons this recipe serves
     */
    private int serving;

    /**
     * Recipe's name
     */
    private final String name;


    /**
     * ArrayList containing nutritional information.
     */
    private final ArrayList<NutritionalInformation> nutritionalInformation;

    /**
     * Creates a recipe.
     * @param name Recipe's name.
     * @see #addNutritionalInformation(NutritionalInformation)
     * @see #addIngredient(String)
     * @see #addRecipeInstruction(String)
     * @see #addRecipeOwner(Chef)
     * @see #setServing(int)
     * @see #setPreparationTime(int)
     */
    public Recipe(@NonNull String name){
        ingredients = new ArrayList<>();
        directions = new ArrayList<>();
        recipeOwners = new ArrayList<>();
        nutritionalInformation = new ArrayList<>();
        this.name = name;
    }

    /**
     * Should not be used, this constructor is intent to be used by Android operational system only.
     * @param in Parcel to read from and initialize variable values.
     */
    protected Recipe(Parcel in) {
        ingredients = in.createStringArrayList();
        directions = in.createStringArrayList();
        recipeOwners = in.createTypedArrayList(Chef.CREATOR);
        preparationTime = in.readInt();
        serving = in.readInt();
        nutritionalInformation = in.createTypedArrayList(NutritionalInformation.CREATOR);
        name = in.readString();
    }

    /**
     * Needed for parcelable implementation.
     */
    public static final Creator<Recipe> CREATOR = new Creator<Recipe>() {
        @Override
        public Recipe createFromParcel(Parcel in) {
            return new Recipe(in);
        }

        @Override
        public Recipe[] newArray(int size) {
            return new Recipe[size];
        }
    };

    /**
     *
     * @return List of ingredients to prepare this recipe
     * @see #addIngredient(String)
     */
    public @NonNull ArrayList<String> getIngredients() {
        return ingredients;
    }
    /**
     *
     * @param ingredient Ingredient to add.
     */
    public void addIngredient(@NonNull String ingredient){
        this.ingredients.add(ingredient);
    }

    /**
     *
     * @return List of instructions to prepare this recipe.
     * @see #addRecipeInstruction(String)
     */
    public @NonNull ArrayList<String> getDirections() {
        return directions;
    }

    /**
     *
     * @param instruction Instruction to add to prepare this recipe
     * @see #getDirections()
     */
    public void addRecipeInstruction(@NonNull String instruction){
        this.directions.add(instruction);
    }

    /**
     *
     * @return List of chefs who prepared this recipe
     * @see #addRecipeOwner(Chef)
     */
    public @NonNull ArrayList<Chef> getRecipeOwners() {
        return recipeOwners;
    }

    /**
     *
     * @param owner Owner to add.
     */
    public void addRecipeOwner(@NonNull Chef owner){
        this.recipeOwners.add(owner);
    }

    /**
     *
     * @return Positive integer number of persons this recipe serves. If {@code 0} or less is
     * returned then the number of persons this recipe can serve is unknown.
     */
    public int getServing() {
        return serving;
    }

    /**
     *
     * @param serving Positive integer number of persons which this recipe can serve. Default
     *                is {@code 0} meaning an unknown number of persons that can be served by
     *                this recipe.
     */
    public void setServing(int serving) {
        this.serving = serving;
    }

    /**
     *
     * @return Time to prepare this recipe in minutes. If {@code 0} or less is returned then
     * preparation time is unknown
     */
    public int getPreparationTime() {
        return preparationTime;
    }

    /**
     *
     * @param preparationTime Time to prepare this recipe in minutes. Default is {@code 0} meaning
     *                        an unknown preparation time.
     */
    public void setPreparationTime(int preparationTime) {
        this.preparationTime = preparationTime;
    }

    /**
     *
     * @return Recipe's name.
     */
    public @NonNull String getName(){
        return this.name;
    }

    /**
     *
     * @return List containing all nutritional information about this recipe.
     */
    public @NonNull ArrayList<NutritionalInformation> getNutritionalInformation() {
        return nutritionalInformation;
    }

    /**
     * @param nutritionalInformation NutritionalInformation of this recipe to add.
     */
    public void addNutritionalInformation(NutritionalInformation nutritionalInformation){
        this.nutritionalInformation.add(nutritionalInformation);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        Chef[] chefs = new Chef[recipeOwners.size()];
        NutritionalInformation[] nutritionalInformation = new NutritionalInformation[this.nutritionalInformation.size()];

        chefs = recipeOwners.toArray(chefs);
        nutritionalInformation = this.nutritionalInformation.toArray(nutritionalInformation);

        dest.writeStringList(ingredients);
        dest.writeStringList(directions);
        dest.writeTypedArray(chefs, 0);
        dest.writeInt(preparationTime);
        dest.writeInt(serving);
        dest.writeTypedArray(nutritionalInformation, 0);
        dest.writeString(name);
    }

}
