package com.amplastudio.eoquetem.recipe.res;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Class that defines basics nutritional information. All information should consider a 2000 kcal diet.
 * @author Murillo Henrique Pedroso Ferreira
 */
public class NutritionalInformation implements Parcelable{

    /**
     * Name of nutritional information (calories, fat, carbs, etc).
     */
    private String name;

    /**
     * Unit of {@link #value} (mg, kcal, g, etc).
     */
    private String valueUnit;

    /**
     * Nutritional information.
     */
    private float value;

    /**
     * Percent representing the {@link #value} on a 2000 kcal diet
     */
    private float dietPercent;

    /**
     *
     * @param name Name of nutrition (calories, fat, carbs, etc).
     * @param value Floating positive value representing a nutritional information.
     * @param valueUnit Unit of the nutritional information (kcal, g, mg, etc).
     * @param dietPercent Percent representing the {@code value} considering a 2000 kcal diet.
     */
    public NutritionalInformation(String name, float value, String valueUnit, float dietPercent){
        this.name = name;
        this.value = value;
        this.valueUnit = valueUnit;
        this.dietPercent = dietPercent;
    }

    /**
     * Should not be used, this constructor is intent to be used by Android operational system only.
     * @param in Parcel to read from and initialize variable values.
     */
    protected NutritionalInformation(Parcel in) {
        name = in.readString();
        valueUnit = in.readString();
        value = in.readFloat();
        dietPercent = in.readFloat();
    }

    /**
     * Needed for parcelable implementation.
     */
    public static final Creator<NutritionalInformation> CREATOR = new Creator<NutritionalInformation>() {
        @Override
        public NutritionalInformation createFromParcel(Parcel in) {
            return new NutritionalInformation(in);
        }

        @Override
        public NutritionalInformation[] newArray(int size) {
            return new NutritionalInformation[size];
        }
    };

    /**
     *
     * @return Name of nutritional information (calories, fat, carbs, etc).
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return Unit of the value returned by {@link #getValue()} (mg, kcal, g, etc).
     */
    public String getValueUnit() {
        return valueUnit;
    }

    /**
     *
     * @return Nutritional information.
     */
    public float getValue() {
        return value;
    }

    /**
     *
     * @return Percent representing the {@link #value} on a 2000 kcal diet
     */
    public float getDietPercent() {
        return dietPercent;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(valueUnit);
        dest.writeFloat(value);
        dest.writeFloat(dietPercent);
    }
}
