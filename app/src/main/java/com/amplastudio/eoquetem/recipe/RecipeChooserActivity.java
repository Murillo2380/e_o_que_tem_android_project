package com.amplastudio.eoquetem.recipe;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.amplastudio.eoquetem.R;
import com.amplastudio.eoquetem.recipe.adapter.RecipeChooserRecyclerViewAdapter;
import com.amplastudio.eoquetem.recipe.res.Recipe;
import com.amplastudio.eoquetem.recipe.util.RecipeLoader;

/**
 * Class that handles multiples recipes so user will be able to select one of then before
 * going to {@link RecipeActivity}. This class must receive an array of ingredients selected
 * in {@link com.amplastudio.eoquetem.MainActivity} because the available recipes will be
 * retrieved based on the chosen ingredients.
 * @see #EXTRA_INGREDIENTS
 * @author Murillo Henrique Pedroso Ferreira.
 */
public class RecipeChooserActivity extends AppCompatActivity implements RecipeLoader.Callback,
        RecipeChooserRecyclerViewAdapter.OnRecipeChosenListener{

    /**
     * Extra key to used when calling this activity. An array of {@link String} must be passed
     * using this key, relative to the ingredients chosen in
     * {@link com.amplastudio.eoquetem.MainActivity}
     */
    public static final String EXTRA_INGREDIENTS = "extra_ingredients";

    /**
     * RecyclerView holding all cards with the available recipes.
     */
    private RecyclerView cardsRecyclerView;

    private RecipeChooserRecyclerViewAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recipe_chooser_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.recipe_chooser_activity_toolbar);
        setSupportActionBar(toolbar);

        String[] ingredients = getIntent().getStringArrayExtra(EXTRA_INGREDIENTS);

        if(ingredients == null){
            Log.e(getResources().getString(R.string.app_name) + " error", "Ingredients array " +
                    "is null, did you passed the list of ingredients using an array of Strings " +
                    "before starting " + getClass().getSimpleName() + " activity?");
        }


        //RecipeLoader recipeLoader = new RecipeLoader(this);
        cardsRecyclerView = (RecyclerView) findViewById(R.id.recipe_chooser_activity_recyclerview);
        cardsRecyclerView.setLayoutManager(new GridLayoutManager(this,
                getResources().getInteger(R.integer.activity_recipe_chooser_grid_columns)));

        adapter = new RecipeChooserRecyclerViewAdapter(this, null);
        Recipe r;
        adapter.addRecipe(RecipeLoader.loadDemoRecipe());
        r = RecipeLoader.loadDemoRecipe();
        r.addIngredient("Teste");
        r.addIngredient("Teste");
        r.addIngredient("Teste");
        adapter.addRecipe(r);
        r = RecipeLoader.loadDemoRecipe();
        r.addIngredient("Teste 2");
        r.addIngredient("Teste 2");
        r.addIngredient("Teste 2");
        r.addIngredient("Teste 2");
        r.addIngredient("Teste 2");
        r.addIngredient("Teste 2");
        r.addIngredient("Teste 2");
        r.addIngredient("Teste 2");
        r.addIngredient("Teste 2");
        r.addIngredient("Teste 2");
        r.addIngredient("Teste 2");
        r.addIngredient("Teste 2");
        r.addIngredient("Teste 2");
        r.addIngredient("Teste 2");
        r.addIngredient("Teste 2");
        r.addIngredient("Teste 2");
        r.addIngredient("Teste 2");
        r.addIngredient("Teste 2");
        r.addIngredient("Teste 2");
        r.addIngredient("Teste 2");
        r.addIngredient("Teste 2");
        r.addIngredient("Teste 2");
        r.addIngredient("Teste 2");
        r.addIngredient("Teste 2");
        adapter.addRecipe(r);


        cardsRecyclerView.setAdapter(adapter);

    }


    @Override
    public void onAllRecipeLoaded(Recipe[] recipes) {

    }

    @Override
    public void onRecipeLoaded(Recipe recipe) {

    }

    @Override
    public void onRecipeChosen(Recipe recipe) {
        Intent intent = new Intent(this, RecipeActivity.class);
        intent.putExtra(RecipeActivity.EXTRA_RECIPE, recipe);
        startActivity(intent);
    }

}
