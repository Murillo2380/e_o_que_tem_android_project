package com.amplastudio.eoquetem.recipe.util;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.amplastudio.eoquetem.recipe.res.Chef;
import com.amplastudio.eoquetem.recipe.res.NutritionalInformation;
import com.amplastudio.eoquetem.recipe.res.Recipe;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Class that loads a recipe.
 * @author Murillo Henrique Pedroso Ferreira
 */
public class RecipeLoader extends AsyncTask<String, Recipe, ArrayList<Recipe>>{

    private Callback callback;

    /**
     *
     * @param callback Callback to be notified when recipe was loaded.
     * @see Callback#onRecipeLoaded(Recipe)
     * @see Callback#onAllRecipeLoaded(Recipe[])
     */
    public RecipeLoader(@NonNull Callback callback){
        this.callback = callback;
    }

    @Override
    protected ArrayList<Recipe> doInBackground(String... ingredients) {

        return null;
    }

    @Override
    protected void onProgressUpdate(Recipe... recipes){
        for(Recipe r : recipes)
            callback.onRecipeLoaded(r);
    }

    @Override
    protected void onPostExecute(ArrayList<Recipe> recipes){
        callback.onAllRecipeLoaded(recipes.toArray(new Recipe[recipes.size()]));
    }


    /**
     * Loads a recipe from a given input stream.
     * @param in InputStream which contains the recipe data.
     * @return Recipe with the information contained in {@code in}
     */
    public static Recipe loadRecipe(InputStream in){

        return null;
    }

    /**
     *
     * @return Demo recipe.
     */
    public @NonNull static Recipe loadDemoRecipe(){

        Recipe recipe = new Recipe("Queijo quente");
        Chef chefExample = new Chef("Chef exemplo");

        recipe.addRecipeOwner(chefExample);
        recipe.addRecipeInstruction("Passar a manteiga em um dos lados do pão, colocar as fatias" +
                " na frigireira para tostar");
        recipe.addRecipeInstruction("Virar a parte tostada para cima e cobrir cada pão com " +
                "queijo cheddar e o queijo prato");
        recipe.addRecipeInstruction("Colocar uma fatia sobre a outra unindo as partes com queijo");
        recipe.addRecipeInstruction("Passar manteiga em ambos os lados do pão e ir virando" +
                " até que o queijo esteja completamente derretido");

        recipe.addIngredient("Pao");
        recipe.addIngredient("Manteiga");
        recipe.addIngredient("Queijo");

        recipe.setPreparationTime(15);
        recipe.setServing(1);

        NutritionalInformation nutritionalInformation = new NutritionalInformation("Calorias",
                200f, "kcal", 10f);
        recipe.addNutritionalInformation(nutritionalInformation);

        return recipe;

    }

    /**
     * Callback for task executor, called when the class {@link RecipeLoader} finished
     * loading one or all the recipes.
     */
    public interface Callback{

        /**
         * Called after all recipes has been loaded (run on UIThread). In case of need to
         * check when each recipe was loaded one by one, see {@link #onRecipeLoaded(Recipe)}
         * @param recipes Array of all recipes loaded.
         */
        void onAllRecipeLoaded(Recipe[] recipes);

        /**
         * Called when a single recipe has been loaded. In case of need to check only when
         * all the other recipes has been downloaded, see {@link #onAllRecipeLoaded(Recipe[])}
         * @param recipe Recipe loaded.
         */
        void onRecipeLoaded(Recipe recipe);

    }


}
