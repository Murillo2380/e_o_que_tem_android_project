package com.amplastudio.eoquetem;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.amplastudio.eoquetem.adapter.IngredientsListAdapter;
import com.amplastudio.eoquetem.recipe.RecipeActivity;
import com.amplastudio.eoquetem.recipe.RecipeChooserActivity;
import com.amplastudio.eoquetem.recipe.util.RecipeLoader;

/**
 * First activity to be instantiated, showing the list of available ingredients to be selected by the user.
 * @author Murillo Henrique Pedroso Ferreira
 */
public class MainActivity extends AppCompatActivity implements Button.OnClickListener{

    /**
     * Used as a key in bundle state.
     */
    private static final String INGREDIENTS_STATE_KEY = "ingredients_key";

    /**
     * List of ingredients to be selected
     */
    private RecyclerView recyclerView;

    private IngredientsListAdapter.Ingredient[] ingredients;

    private IngredientsListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.colorToolbarTitle));
        setSupportActionBar(toolbar);

        Button button = (Button) findViewById(R.id.activity_main_button_selection_completed);
        button.setOnClickListener(this);

        recyclerView = (RecyclerView) findViewById(R.id.activity_main_recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));

        if(savedInstanceState == null) {
            String[] defaultIngredients = getResources().getStringArray(R.array.ingredients);
            ingredients = new IngredientsListAdapter
                    .Ingredient[defaultIngredients.length];

            for (int i = 0; i < defaultIngredients.length; i++)
                ingredients[i] = new IngredientsListAdapter.Ingredient(defaultIngredients[i]);
        }

        else{
            ingredients = (IngredientsListAdapter.Ingredient[]) savedInstanceState.getParcelableArray(INGREDIENTS_STATE_KEY);
        }

        adapter = new IngredientsListAdapter(ingredients);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        outState.putParcelableArray(INGREDIENTS_STATE_KEY, ingredients);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.activity_main_button_selection_completed:

                String[] selectedIngredients = adapter.getSelectedIngredients();

                if(selectedIngredients == null){
                    Toast.makeText(this, R.string.activity_main_message_select_ingredients,
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent intent = new Intent(this, RecipeChooserActivity.class);
                intent.putExtra(RecipeChooserActivity.EXTRA_INGREDIENTS, selectedIngredients);
                startActivity(intent);
                break;
        }

    }

}
