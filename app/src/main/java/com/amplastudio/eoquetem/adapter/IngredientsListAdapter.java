package com.amplastudio.eoquetem.adapter;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.amplastudio.eoquetem.R;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Adapter to display ingredients to seach for a recipe.
 */
public class IngredientsListAdapter extends RecyclerView.Adapter<IngredientsListAdapter.ViewHolder>
        implements CompoundButton.OnCheckedChangeListener{



    class ViewHolder extends RecyclerView.ViewHolder{

        CheckBox ingredientCheckBox;
        TextView ingredientNameTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ingredientCheckBox = (CheckBox) itemView.findViewById(R.id.ingredient_list_item_checkbox);
            ingredientNameTextView = (TextView) itemView.findViewById(R.id.ingredient_list_item_textview);
            ingredientCheckBox.setOnCheckedChangeListener(IngredientsListAdapter.this);
        }

    }

    /**
     * Inner class that defines some attributes used by this adapter to correctly shows to the user
     * if a specific ingredient was selected or not, due to view recycling that may occur while
     * the user is scrolling the list up or down.
     */
    public static class Ingredient implements Parcelable{
        private boolean isSelected;
        private String name;

        /**
         *
         * @param name Ingredient's name.
         */
        public Ingredient(String name){
            this.name = name;
        }

        public Ingredient(Parcel source){
            this.isSelected = source.readByte() == 1;
            this.name = source.readString();
        }

        /**
         *
         * @return {@code true} If this ingredient was selected in the list.
         */
        public boolean isSelected() {
            return isSelected;
        }

        /**
         *
         * @param selected {@code true} if this ingredient must be used to search for a recipe.
         */
        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        /**
         *
         * @return The name of the ingredient.
         */
        public String getName() {
            return name;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeByte((byte) (isSelected ? 1 : 0));
            dest.writeString(name);
        }

        public static final Creator<Ingredient> CREATOR = new Creator<Ingredient>() {
            @Override
            public Ingredient createFromParcel(Parcel source) {
                return new Ingredient(source);
            }

            @Override
            public Ingredient[] newArray(int size) {
                return new Ingredient[size];
            }
        };
    }

    /**
     * List of ingredients that can be used to search for recipes.
     */
    private Ingredient[] ingredients;

    /**
     *
     * @param ingredients Ingredients to be shown as an option to the user select to search for recipes.
     */
    public IngredientsListAdapter(@NonNull Ingredient[] ingredients){
        this.ingredients = ingredients;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ingredient_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.ingredientCheckBox.setTag(holder.getAdapterPosition());
        holder.ingredientCheckBox.setChecked(ingredients[position].isSelected);
        holder.ingredientNameTextView.setText(ingredients[position].getName());
    }

    @Override
    public int getItemCount() {
        return ingredients.length;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int adapterPosition = (int) buttonView.getTag();
        ingredients[adapterPosition].setSelected(isChecked);

    }

    /**
     * Iterate over all checkboxes and return a list containing all ingredients that was
     * selected.
     * @return Array of the selected ingredients or {@code null} if no ingredients was selected.
     */
    public @Nullable String[] getSelectedIngredients(){

        ArrayList<String> selectedIngredients = new ArrayList<>();

        for(Ingredient i : ingredients)
            if(i.isSelected)
                selectedIngredients.add(i.getName());

        if(selectedIngredients.isEmpty())
            return null;

        String[] selectedIngredientsArray = new String[selectedIngredients.size()];
        return selectedIngredients.toArray(selectedIngredientsArray);
    }

}
